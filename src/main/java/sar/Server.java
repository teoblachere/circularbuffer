package sar;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Allows communications between clients in a Client/Server architecture
 *      - All communications between clients must go through an object of this type
 * Channels are used between Clients and Server on a given port for communication
 */
public class Server {

    List<IChannel> connectedClientChannels; // List of channels connected to the server
    Broker broker;
    int port;
    int numberOfConnectedClients;
    final int CLIENT_LIMIT = 3; // Max number of clients that can be connected to server

    public Server(Broker broker, int port) {
        this.broker = broker;
        this.port = port;
        connectedClientChannels = new ArrayList<>();
    }

    public void run() {
        // attempts to accept connections on the broker for a given number of clients (CLIENT_LIMIT)
        while (numberOfConnectedClients < CLIENT_LIMIT) {
            // Call accept and wait for new channel
            IChannel newChannel = broker.accept(port);
            // Once added, add channel to map and create a thread for the channel
            connectedClientChannels.add(newChannel);
            // Once a connection has been established for a single client, run a new thread for a single client channel using a ChannelListener object
            // This allows for checks for information sent from each client in parallel
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // Will listen to any changes (writes) to the client channel
                    ChannelListener clientChannelListener = new ChannelListener(newChannel);
                    clientChannelListener.run();
                    System.out.println("Connection established with client no = " + numberOfConnectedClients);
                }
            }, "client" + numberOfConnectedClients +1).start();
            numberOfConnectedClients++;


        }


    }

    /*
     * Sends textArea from a client channel to other other client channels
     * Synchronized to make sure changes not broadcasted at the same time for the same instance
     *      - This is important given the nature of channel communication.
     *          - The buffer should be filled consecutively with object length information then the serializable object
     *          - This process should not be done in parrallel with other threads as this could corrupt the serializable object read on the other side of the channel
     */
    public synchronized void broadCastChange(IChannel sendingChannel, byte[] serializedObjToSend){
        for(IChannel channel : connectedClientChannels){
            if(channel != sendingChannel){
                try{
                    int byteWritten = channel.write(new byte[]{(byte) serializedObjToSend.length}, 0, 1);
                    if (byteWritten < 0) {
                        throw new IOException("closed on object size write");
                    }
                    int bytesWrittenTotal = 0;
                    while(bytesWrittenTotal < serializedObjToSend.length) {
                        int bytesWritten = channel.write(serializedObjToSend, bytesWrittenTotal, serializedObjToSend.length - bytesWrittenTotal);
                        if (bytesWritten < 0) {
                            throw new IOException("closed on serialized object write");
                        }
                        bytesWrittenTotal += bytesWritten;
                    }
                } catch (IOException ioException){
                    ioException.printStackTrace();
                }

            }
        }
    }

    class ChannelListener implements Runnable {

        IChannel clientChannel;

        ChannelListener(IChannel connectedClientChannel){
            this.clientChannel = connectedClientChannel;
        }

        @Override
        public void run() {

            byte[] readSizeBuffer = new byte[1];

            // Constant loop, reading any textArea changes sent by the client channel
            while (true) {

                try{
                    // within the channel, there is a constant loop waiting for information to be entered into the buffer of the channel
                    // the thread is stuck on the following line until information has been entered into the buffer
                    int byteRead = clientChannel.read(readSizeBuffer, 0,1);
                    if (byteRead < 0) {
                        throw new IOException("closed on object size read");
                    }

                    // Get size of byte array that will contain serialized object
                    int readSize = Byte.toUnsignedInt(readSizeBuffer[0]);

                    byte[] serializedObject = new byte[readSize];

                    int offset = 0;
                    int remaining = serializedObject.length;
                    while (remaining > 0) {
                        int bytesRead = clientChannel.read(serializedObject, offset, remaining);
                        if (bytesRead < 0) {
                            throw new IOException("closed on serialized object read");
                        }
                        offset += bytesRead;
                        remaining -= bytesRead;
                    }

                    // Broadcast change to other clients via Server instance shared between all ChannelListener threads
                    broadCastChange(this.clientChannel, serializedObject);
                } catch (IOException ioException){
                    ioException.printStackTrace();
                }
            }

        }
    }
}
