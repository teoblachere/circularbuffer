package sar;

import java.util.concurrent.ConcurrentHashMap;

public class Broker implements IBroker{

    private final int capacity;

    private ConcurrentHashMap<Integer, IChannel> waitingConnection;

    public Broker(int capacity) {
        this.capacity = capacity;
        waitingConnection = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized IChannel accept(int port) {
        Channel channel = new Channel(capacity);
        Channel otherChannel = new Channel(capacity);
        channel.setReadChannel(otherChannel);
        otherChannel.setReadChannel(channel);

        waitingConnection.put(port, otherChannel);
        while(waitingConnection.containsKey(port)){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return channel;
    }

    @Override
    public synchronized IChannel connect(int port) {
        while(!waitingConnection.containsKey(port)){
            try {
                wait(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        IChannel channel = waitingConnection.get(port);
        waitingConnection.remove(port);
        notifyAll();
        return channel;
    }
}
