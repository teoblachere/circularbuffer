package sar;

public class ArrayCircularBuffer extends CircularBuffer {

    byte[] m_buffer;
    int capacity;
    int pull_pos;
    int push_pos;
    int current_buffer_size;

    public ArrayCircularBuffer(int bufferCapacity) {
        m_buffer = new byte[bufferCapacity];
        capacity = bufferCapacity;
        pull_pos = 0;
        push_pos = 0;
        current_buffer_size = 0;
    }

    @Override
    public synchronized boolean full() {
        return current_buffer_size == capacity;
    }

    @Override
    public synchronized boolean empty() {
        return current_buffer_size == 0;
    }

    @Override
    public synchronized void push(byte b) {
        if(full()){
            throw new IllegalStateException();
        }else{
            m_buffer[push_pos] = b;
            push_pos = incrementPosition(push_pos);
            current_buffer_size++;
        }
    }

    @Override
    public synchronized byte pull() {
        if(empty()){
            throw new IllegalStateException();
        }else{
            byte b = m_buffer[pull_pos];
            pull_pos = incrementPosition(pull_pos);
            current_buffer_size--;
            return b;
        }
    }

    private int incrementPosition(int position){
        if(position == capacity-1){
            return 0;
        }else{
            return position+1;
        }
    }
}
