package sar;


import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/*
 * Run by the user to start the program
 */
public class HackText {

    public static void main(String args[]) {

        // Sets up the AWT GUI frame with the contained text areas
        Frame frame = new Frame();
        frame.addWindowListener(new _WindowListener());

        new HackText(frame);

        frame.setSize(600, 800);
        frame.pack();
        frame.setVisible(true);
    }

    HackText(Frame frame) {
        // Initialises the broker to be used by the server/clients
        // Channel size big enough to not slow performance (can contain average size of TextModificationData serializable object and another byte for size info ~161 bytes)
        final Broker broker = new Broker(200);
        frame.setLayout(new FlowLayout());

        for (int i = 0; i < 3; i++) {
            TextArea text = new TextArea("Welcome!");
            text.setPreferredSize(new Dimension(400, 400));
            frame.add(text);
            // For each text area, Client object is run on a new thread containing the keyListener for the textArea
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Client c = new Client(text, broker, 80);
                    c.run();
                }
            }, "client" + i).start();
        }

        // A Server object is run on a new thread, used for communication between the clients
        new Thread(new Runnable() {
            @Override
            public void run() {
                Server s = new Server(broker, 80);
                s.run();
            }
        }, "server").start();

    }

    static class _WindowListener implements WindowListener {

        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }

        @Override
        public void windowClosed(WindowEvent e) {
            System.exit(0);
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }

    }

}