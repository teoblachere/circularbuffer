package sar;

import java.io.Serializable;

enum Action {
    WRITE,
    DELETE,
    BACKSPACE
}

/*
 * Contains information of a change in a TextArea
 */
public class TextModificationData implements Serializable {

    private char character;
    private int offset;
    private Action action;

    public TextModificationData(char character, int offset, Action action) {
        this.character = character;
        this.offset = offset;
        this.action = action;
    }

    public TextModificationData(int offset, Action action) {
        this.offset = offset;
        this.action = action;
    }

    public char getCharacter() {
        return character;
    }

    public int getOffset() {
        return offset;
    }

    public Action getAction() {
        return action;
    }
}
