package sar;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;

/*
 * - The client has two principal roles
 *      - Process any AWT GUI events in the EDT
 *      - Detect any textArea changes from other clients via the server
 */
public class Client extends Component implements KeyListener, Runnable {

    private IChannel channel;
    private TextArea textArea;
    private static EventQueue evtq;

    public Client(TextArea textArea, IBroker broker, int port) {

        // Connect to server
        while(channel == null){
            channel = broker.connect(port);
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex){}
        }

        // textArea component related to client
        this.textArea = textArea;

        // Get event queue for the EDT and enable a custom AWT event to handle textArea changes sent from other clients (via the server)
        evtq = Toolkit.getDefaultToolkit().getSystemEventQueue();
        enableEvents(TextModificationEvent.EVENT_ID);

        // Process key listener events on the EDT
        textArea.addKeyListener(this);
    }

    private boolean ignoredKey(int code) {
        return (code == KeyEvent.VK_LEFT) || (code == KeyEvent.VK_RIGHT)
                || (code == KeyEvent.VK_UP) || (code == KeyEvent.VK_DOWN)
                || (code == KeyEvent.VK_PAGE_UP) || (code == KeyEvent.VK_PAGE_DOWN)
                || (code == KeyEvent.VK_SHIFT);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    // Event triggered when user presses key in window
    @Override
    public void keyPressed(KeyEvent e) {

        // Text object
        TextArea src = (TextArea) e.getSource();
        // Position of text occurence
        int offset = src.getCaretPosition();
        // Key number
        int code = e.getKeyCode();
        TextModificationData data = null;
        if (code == KeyEvent.VK_BACK_SPACE) {
            if (offset > 0) {
                data = new TextModificationData(offset, Action.BACKSPACE);
            }
        } else if (code == KeyEvent.VK_DELETE) {
            String text = src.getText();
            if (offset < text.length()){
                data = new TextModificationData(offset, Action.DELETE);
            }
        } else if (!ignoredKey(code)) {
            data = new TextModificationData(e.getKeyChar(), offset, Action.WRITE);
        }

        // Send changes to server if event called from this client
        if(data != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {

                System.out.println("\n\nSend action to server");
                System.out.println("action = " + data.getAction());
                System.out.println("char = " + data.getCharacter());
                System.out.println("offset = " +data.getOffset());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(data);
                objectOutputStream.flush();
                byte[] bytes = byteArrayOutputStream.toByteArray();
                //Write to channel, first send array size, and then send the array
                int byteWritten = channel.write(new byte[]{(byte) bytes.length}, 0, 1);
                if (byteWritten < 0) {
                    throw new IOException("closed on object size write");
                }

                int bytesWrittenTotal = 0;
                while(bytesWrittenTotal < bytes.length) {
                    int bytesWritten = channel.write(bytes, bytesWrittenTotal, bytes.length - bytesWrittenTotal);
                    if (bytesWritten < 0) {
                        throw new IOException("closed on serialized object write");
                    }
                    bytesWrittenTotal += bytesWritten;
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void run(){
        while(true) {
            try{
                // One thread reads for one channel at a time
                byte[] readLengthArray = new byte[1];

                int byteRead = channel.read(readLengthArray, 0,1);
                if (byteRead < 0) {
                    throw new IOException("closed on object size read");
                }

                int readLength = Byte.toUnsignedInt(readLengthArray[0]);
                byte[] bytes = new byte[readLength];
                int readCount = 0;
                while(readCount < readLength){
                    int bytesRead = channel.read(bytes, readCount, readLength - readCount);
                    if(bytesRead < 0){
                        throw new IOException("closed on object size read");
                    }
                    readCount += bytesRead;
                }
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                ObjectInputStream objectInputStream = null;
                TextModificationData data = null;
                try {
                    objectInputStream = new ObjectInputStream(byteArrayInputStream);
                    data = (TextModificationData) objectInputStream.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                if(data != null){
                    // Create AWT event and post to the event queue and handled by the EDT for the textArea change
                    TextModificationEvent event = new TextModificationEvent(this, data);
                    evtq.postEvent(event);
                }
            } catch (IOException ioException){
                ioException.printStackTrace();
            }
        }
    }

    /*
     * Event posted by client when it receives a change
     */
    public void processEvent(AWTEvent event){
        if(event instanceof TextModificationEvent){
            TextModificationData data = ((TextModificationEvent) event).getData();
            System.out.println("\n\nProcess action from other client (sent via the server)");
            System.out.println("action = " + data.getAction());
            System.out.println("char = " + data.getCharacter());
            System.out.println("offset = " +data.getOffset());
            switch(data.getAction()){
                case DELETE:
                    textArea.replaceRange("", data.getOffset(), data.getOffset() + 1);
                    break;
                case BACKSPACE:
                    textArea.replaceRange("", data.getOffset(), data.getOffset() - 1);
                    break;
                case WRITE:
                    textArea.getText().length(); // stops java.lang.StringIndexOutOfBoundsException being thrown
                    textArea.insert(String.valueOf(data.getCharacter()), data.getOffset());
                    break;
            }
        }else{
            super.processEvent(event);
        }
    }
}
