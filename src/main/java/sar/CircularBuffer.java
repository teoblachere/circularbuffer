package sar;

/**
 * This circular buffer of bytes can be used to pass bytes between two threads:
 * one thread pushing bytes in the buffer and the other pulling bytes from the
 * buffer. The buffer policy is lossless and FIFO (first byte in is the first byte out).
 */
public abstract class CircularBuffer {
    /**
     * @return true if this buffer is full, false otherwise
     */
    public abstract boolean full();
    /**
     * @return true if this buffer is empty, false otherwise
     */
    public abstract boolean empty();
    /**
     * @param b: the next byte to push in the buffer
     * @throws an IllegalStateException if full.
     */
    public abstract void push(byte b);
    /**
     * @return the next available byte
     * @throws an IllegalStateException if empty.
     */
    public abstract byte pull();
}