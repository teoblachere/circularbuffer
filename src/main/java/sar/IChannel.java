package sar;


/**
 * A channel is bidirectional byte stream between two threads, represented
 * as a pair of cooperating channel objects, with one channel object
 * at either end of the stream. In other words, one thread is using one
 * of the channel object and the other thread is using the other.
 *
 * Once established, a channel can be used to exchange bytes between the two
 * connected threads, by writing or reading bytes to and from the channel. A channel
 * is FIFO and lossless.
 *
 * When reading, a channel will provide available bytes, if there are any. If no
 * bytes are available, the read operation will block.
 *
 * When writing, a channel will accept bytes, if there is room to do so. If
 * there is no room at all, when the write operation starts, the operation will
 * block. If there is room, whatever bytes can be written are written and the
 * operation returns the number of bytes written.
 *
 */
public interface IChannel {
    /**
     * @return true if the channel is empty, that is, there is nothing to read and
     * any attempt to read some bytes would block. Returns false
     * otherwise.
     */
    public boolean empty();
    /**
     * @return true if the channel is full, that is, there is no room to write any
     * more bytes and any attempt to write some bytes would block. Returns
     * false otherwise.
     */
    public boolean full();
    /**
     * This method attempts to read bytes from this channel. It blocks if there
     * are no bytes to read, that is, the method "empty()" would return true. The
     * read bytes are stored in the given buffer, starting at the given offset. At
     * most, count bytes will be read.
     *
     * The ownership of the buffer is transferred for the duration of this method.
     * No reference is kept to this buffer internally once the method returns.
     *
     * @param buffer: the buffer to store read bytes
     * @param offset: the offset in the buffer to start at
     * @param count: the count of bytes to be copied
     * @return the number of bytes read, -1 if this socket is closed.
     */
    public abstract int read(byte[] buffer, int offset, int count);
    /**
     * This method attempts to write bytes to this channel, if there is room to do
     * so; it blocks otherwise. The bytes are copied from the given buffer, starting
     * at the given offset. At most, count bytes will be written.
     *
     * More precisely, if there is no room at all, when the write operation
     * starts, the operation will block. In other words, if the method "full()"
     * returns true, this write operation will block until there is room to write
     * at least one byte. If there is room to write at least one byte, when the
     * write operation starts, whatever bytes can be written are written and the
     * operation returns the number of bytes written.
     *
     * The ownership of the buffer is transferred for the duration of this method.
     * No reference is kept to this buffer internally once the method returns.
     *
     * @param buffer: the buffer to store read bytes
     * @param offset: the offset in the buffer to start at
     * @param count: the count of bytes to be copied
     * @return the number of bytes written or -1 if this socket is closed.
     */
    public abstract int write(byte[] buffer, int offset, int count);
    /**
     * Closes this side of the channel. Once closed, read and write operations are
     * no longer permitted on this side. However, any byte written from this side,
     * prior to closing this side of the channel, will be available to read to
     * the other side.
     * Closing a channel multiple times is harmless and ignored.
     */
    public abstract void close();
}