package sar;

public class Channel implements IChannel {

    public ArrayCircularBuffer circularBuffer;
    private Channel readChannel;
    private boolean isClosed;

    public Channel(int bufferSize) {
        this.circularBuffer = new ArrayCircularBuffer(bufferSize);
        this.isClosed = false;
    }

    @Override
    public boolean empty() {
        return readChannel.circularBuffer.empty();
    }

    @Override
    public boolean full() {
        return circularBuffer.full();
    }

    @Override
    public synchronized int read(byte[] buffer, int offset, int count) {
        // If closed, or parameters (offset or count) are invalid return -1
        if(isClosed || offset < 0 || count < 0){
            return -1;
        }
        // If empty, wait 10 milliseconds before checking again
        while(empty()){
            try {
                wait(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // If circular buffer not empty, and position in given buffer smaller than the count given, read byte in circular buffer and increment read count
        int readCount = 0;
        while(!empty() && readCount < count){
            buffer[offset + readCount] = readChannel.circularBuffer.pull();
            readCount++;
        }
        return readCount;
    }

    @Override
    public synchronized int write(byte[] buffer, int offset, int count) {
        // If closed, or parameters (offset or count) are invalid or the the last byte to read is out of the range, return -1
        if(isClosed || offset < 0 || count < 0 || (offset + count) > buffer.length){
            return -1;
        }
        // If full, wait 10 milliseconds before checking again
        while(full()){
            try {
                wait(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int writeCount = 0;

        // If circular buffer not full, and position in given buffer smaller than the count given, write byte to circular buffer and increment write count
        while(!full() && writeCount < count){
            circularBuffer.push(buffer[offset + writeCount]);
            writeCount++;
        }

        return writeCount;
    }

    @Override
    public void close() {
        isClosed = true;
    }

    public void setReadChannel(Channel readChannel) {
        this.readChannel = readChannel;
    }
}
