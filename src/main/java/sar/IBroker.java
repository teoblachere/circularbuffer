package sar;

/**
 * A broker that brokers channels between pairs of participating threads.
 * For each pair, one thread accepts connection on a port and other threads
 * connect to that port.
 *
 * Establishing a stream is a rendez-vous that can be initiated by either
 * side: the accepting side or the connecting side. In other words, it is not
 * necessary to ensure that the accept happens before the connect.
 *
 * A different channel is created for each connection. A channel is
 * bidirectional FIFO/lossless byte stream between two threads.
 * Thus, each thread has one channel object, representing one end of the
 * stream. Each thread can use its channel object to either read or write,
 * or both.
 *
 * This broker is thread-safe.
 */
public interface IBroker {
    /**
     * This is a blocking call, awaiting a connect on the given port. It returns a
     * channel object that is connected and can be used to read or write bytes.
     * This method is thread-safe.
     *
     * @param port
     * @return
     */
    public IChannel accept(int port);
    /**
     * Attempt to connect to the given port. If there is an accept on that port, a
     * communication channel is establish. The returned channel is ready to either
     * read or write bytes. This method is thread-safe.
     *
     * @param port
     * @return
     */
    public IChannel connect(int port);
}

