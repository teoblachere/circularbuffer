package sar;

import java.awt.*;

/*
 * Custom AWT event that contains TextModificationData information to be sent to the EDT thread
 */
public class TextModificationEvent extends AWTEvent {

    public static final int EVENT_ID = AWTEvent.RESERVED_ID_MAX + 1;
    private TextModificationData data;

    public TextModificationEvent(Client client, TextModificationData data) {
        super(client, EVENT_ID);
        this.data = data;
    }

    public TextModificationData getData() {
        return data;
    }
}
