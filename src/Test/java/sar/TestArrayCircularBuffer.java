package sar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestArrayCircularBuffer {

    @Test
    public void testPushAndPull(){
        ArrayCircularBuffer buffer = new ArrayCircularBuffer(1);
        byte expected = (byte) 1;
        byte actual;
        buffer.push(expected);
        Assertions.assertTrue(buffer.full());
        actual = buffer.pull();
        Assertions.assertFalse(buffer.full());
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testPushingUntilFull(){
        ArrayCircularBuffer buffer = new ArrayCircularBuffer(2);
        Assertions.assertTrue(buffer.empty());
        Assertions.assertFalse(buffer.full());
        Assertions.assertDoesNotThrow(() -> buffer.push((byte) 1));
        Assertions.assertFalse(buffer.empty());
        Assertions.assertFalse(buffer.full());
        Assertions.assertDoesNotThrow(() -> buffer.push((byte) 2));
        Assertions.assertTrue(buffer.full());
        Assertions.assertThrows(IllegalStateException.class, () -> buffer.push((byte) 3));
    }

    @Test
    public void testPullInEmptyBuffer(){
        ArrayCircularBuffer buffer = new ArrayCircularBuffer(2);
        Assertions.assertThrows(IllegalStateException.class, () -> buffer.pull());
    }

    @Test
    public void testLoopAround(){
        ArrayCircularBuffer buffer = new ArrayCircularBuffer(2);

        byte expected1 = (byte) 1;
        byte expected2 = (byte) 2;
        byte expected3 = (byte) 3;
        buffer.push(expected1);
        buffer.push(expected2);

        byte actual1 = buffer.pull();
        Assertions.assertEquals(expected1, actual1);

        Assertions.assertDoesNotThrow(() -> buffer.push(expected3));
        byte actual2 = buffer.pull();
        Assertions.assertEquals(expected2, actual2);

        byte actual3 = buffer.pull();
        Assertions.assertEquals(expected3, actual3);
    }

}
